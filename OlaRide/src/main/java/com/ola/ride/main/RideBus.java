package com.ola.ride.main;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gcm.demo.olaride.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RideBus.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RideBus#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RideBus extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    static FrameLayout lv;
    ListView listview;
    CardView cv;
    static int done_index=0;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter cit.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RideBus.
     */
    // TODO: Rename and change types and number of parameters
    public static RideBus newInstance(String param1, String param2) {
        RideBus fragment = new RideBus();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public RideBus() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }
    public static void swap(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("deb","inside create");
        View v;
        v =  inflater.inflate(R.layout.fragment_ride_bus_init, container, false);
        lv = (FrameLayout)v.findViewById(R.id.bus_live);
        lv.setVisibility(View.VISIBLE);
        listview = (ListView)v.findViewById(R.id.listView);
        cv = (CardView)v.findViewById(R.id.card_view3);
        String[] values = new String[] { "HSR Layout 7th Sector","Silk Board Junction","Bommanahali","G B Palya","Electronic City" };
        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }
        final MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), values);
        listview.setAdapter(adapter);

        lv.setVisibility(View.GONE);
        BroadcastReceiver bReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(RideSplit.RECEIVE_JSON)) {
                    String serviceJsonString = intent.getBundleExtra("data").getString("action");
                    if(serviceJsonString.equalsIgnoreCase("check_n_bus")){
                        Log.d("data displayed in bus ride",serviceJsonString);
                        done_index++;
                        adapter.notifyDataSetChanged();

                    }


                }
            }
        };
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(getActivity());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RideSplit.RECEIVE_JSON);
        bManager.registerReceiver(bReceiver, intentFilter);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
    private class MySimpleArrayAdapter extends ArrayAdapter<String>{
        private final Context context;
        private final String[] values;

        public MySimpleArrayAdapter(Context context, String[] values) {
            super(context, R.layout.list_item, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.list_item, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.place);

            textView.setText(values[position]);
            if(position<done_index)textView.setTextColor(Color.RED);

            return rowView;
        }

    }

}
